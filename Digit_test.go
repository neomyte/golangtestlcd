package main

import "testing"

func TestDigitFromInt(t *testing.T) {
	assertResult := func(input int, expectedResult string) {
		d := NewDigitFromInt(input)
		result := d.String()
		if result == expectedResult {
			t.Logf("NewDigitFromInt(%d) succeeded.\n", input)
		} else {
			t.Errorf("NewDigitFromInt(%d) failed.\nExpected:\n%v\nGot:\n%v\n", input, expectedResult, result)
		}
	}
	assertResult(10, " _ \n| |\n|_|")
	assertResult(-1, " _ \n| |\n|_|")
	assertResult(0, " _ \n| |\n|_|")
	assertResult(1, "   \n  |\n  |")
	assertResult(2, " _ \n _|\n|_ ")
	assertResult(3, " _ \n _|\n _|")
	assertResult(4, "   \n|_|\n  |")
	assertResult(5, " _ \n|_ \n _|")
	assertResult(6, " _ \n|_ \n|_|")
	assertResult(7, " _ \n  |\n  |")
	assertResult(8, " _ \n|_|\n|_|")
	assertResult(9, " _ \n|_|\n _|")
}

func TestDigitWithSize(t *testing.T) {
	assertResult := func(inputDigit int, inputHeight int, inputWidth int, expectedResult string) {
		d := NewDigitWithSize(inputDigit, inputHeight, inputWidth)
		result := d.String()
		if result == expectedResult {
			t.Logf("NewDigitWithSize(%d, %d, %d) succeeded.\n", inputDigit, inputHeight, inputWidth)
		} else {
			t.Errorf("NewDigitWithSize(%d, %d, %d) failed.\nExpected:\n%v\nGot:\n%v\n", inputDigit, inputHeight, inputWidth, expectedResult, result)
		}
	}
	assertResult(0, 2, 6, " ______ \n|      |\n|      |\n|      |\n|______|")
	assertResult(-10, 3, 2, " __ \n|  |\n|  |\n|  |\n|  |\n|  |\n|__|")
	assertResult(10, 1, 1, " _ \n| |\n|_|")
	assertResult(1, 4, 4, "      \n     |\n     |\n     |\n     |\n     |\n     |\n     |\n     |")
	assertResult(2, 2, 3, " ___ \n    |\n ___|\n|    \n|___ ")
	assertResult(3, 6, 6, " ______ \n       |\n       |\n       |\n       |\n       |\n ______|\n       |\n       |\n       |\n       |\n       |\n ______|")
	assertResult(4, 3, 8, "          \n|        |\n|        |\n|________|\n         |\n         |\n         |")
	assertResult(5, 2, 6, " ______ \n|       \n|______ \n       |\n ______|")
	assertResult(6, 3, 3, " ___ \n|    \n|    \n|___ \n|   |\n|   |\n|___|")
	assertResult(7, 4, 7, " _______ \n        |\n        |\n        |\n        |\n        |\n        |\n        |\n        |")
	assertResult(8, 10, 10, " __________ \n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|__________|\n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|          |\n|__________|")
	assertResult(9, 1, 5, " _____ \n|_____|\n _____|")
}
