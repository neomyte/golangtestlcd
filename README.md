# Description

This projects aim at creating functions that can translate a given number to its corresponding LCD representation.

For instance, a **2** would become:
```
 _ 
 _|
|_
```

The height and width may also be provided, the same **2**, with a height of 2 and a width of 3 would become:
```
 ___ 
    |
 ___|
|
|___
```

# Files

There are five files included in this directory:
* **Digit.go**: Contains all types and functions used to handle a single digit
* **Digits.go**: Contains all types and fonctions used to handle multiple digits
* **Digit_test.go**: Contains all the tests for Digit.go
* **Digits_test.go**: Contains all the tests for Digits.go
* **README.md**: This read me

# Compilation + Test

`go test -cover`