package main

import "testing"

func TestDigitsFromInt( t *testing.T ) {
	assertResult := func (input int, expectedResult string) {
		d := NewDigitsFromInt(input)
		result := d.String()
		if result == expectedResult {
			t.Logf("NewDigitsFromInt(%d) succeeded.\n", input)
		} else {
			t.Errorf("NewDigitsFromInt(%d) failed.\nExpected:\n%v\nGot:\n%v\n", input, expectedResult, result)
		}
	}
	assertResult(10, "    _ \n  || |\n  ||_|\n")
	assertResult(21, " _    \n _|  |\n|_   |\n")
	assertResult(34, " _    \n _||_|\n _|  |\n")
	assertResult(56, " _  _ \n|_ |_ \n _||_|\n")
	assertResult(78, " _  _ \n  ||_|\n  ||_|\n")
	assertResult(93, " _  _ \n|_| _|\n _| _|\n")
	assertResult(1234567890, "    _  _     _  _  _  _  _  _ \n  | _| _||_||_ |_   ||_||_|| |\n  ||_  _|  | _||_|  ||_| _||_|\n")
	assertResult(-1, "   \n  |\n  |\n")
	assertResult(0, " _ \n| |\n|_|\n")
	assertResult(1, "   \n  |\n  |\n")
	assertResult(2, " _ \n _|\n|_ \n")
	assertResult(3, " _ \n _|\n _|\n")
	assertResult(4, "   \n|_|\n  |\n")
	assertResult(5, " _ \n|_ \n _|\n")
	assertResult(6, " _ \n|_ \n|_|\n")
	assertResult(7, " _ \n  |\n  |\n")
	assertResult(8, " _ \n|_|\n|_|\n")
	assertResult(9, " _ \n|_|\n _|\n")
}

func TestDigitsWithSize(t *testing.T) {
	assertResult := func(digits Digits, expectedResult string) {
		result := digits.String()
		if result == expectedResult {
			t.Logf("NewDigitsWithSize succeeded.\n")
		} else {
			t.Errorf("NewDigitsWithSize failed.\nExpected:\n%v\nGot:\n%v\n", expectedResult, result)
		}
	}
	ds := new(Digits)
	ds.AddDigit(NewDigitWithSize(5, 2, 6))
	ds.AddDigit(NewDigitFromInt(5))
	ds.AddDigit(NewDigitWithSize(8, 3, 6))
	ds.AddDigit(NewDigitWithSize(7, 4, 8))
	ds.AddDigits(NewDigitsWithSize(13, 4, 6))
	ds.AddDigits(NewDigitsWithSize(-4, 2, 4))
	ds.AddDigit(NewDigitWithSize(3, 3, 3))
	ds.AddDigits(NewDigitsWithSize(514, 4, 6))
	assertResult(*ds, "                    ________          ______             ______                 \n                            |       |       |           |              ||      |\n            ______          |       |       |       ___ |              ||      |\n           |      |         |       |       |          ||              ||      |\n ______    |      |         |       | ______|          ||______        ||______|\n|          |______|         |       |       ||    | ___|       |       |       |\n|______  _ |      |         |       |       ||____|    |       |       |       |\n       ||_ |      |         |       |       |     |    |       |       |       |\n ______| _||______|         |       | ______|     | ___| ______|       |       |\n")
}
