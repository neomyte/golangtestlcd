package main

// A "Digits" is an array of Digit
// It also possess a height that matchs the height of its highest Digit
type Digits struct {
	digits []Digit
	height int
}

func updateHeight(ds *Digits, height int) {
	if height > ds.height {
		ds.height = height
	}
}

// Append a Digit to the Digits
// If that Digit is higher than the highest of the Digits, the height is updated
func (ds *Digits) append(d Digit) {
	updateHeight(ds, d.Height())
	ds.digits = append(ds.digits, d)
}

// Prepend a Digit to the Digits
// If that Digit is higher than the highest of the Digits, the height is updated
func (ds *Digits) prepend(d Digit) {
	updateHeight(ds, d.Height())
	ds.digits = append([]Digit{d}, ds.digits...)
}

// Add a Digits to the Digits
func (ds *Digits) AddDigit(digit Digit) {
	ds.prepend(digit)
}

// Add a Digits to the Digits
func (ds *Digits) AddDigits(digits Digits) {
	for i := len(digits.digits) - 1; i >= 0; i-- {
		ds.prepend(digits.digits[i])
	}
}

// Create and return a Digits using an integer and a specific size (height, width)
// If a negative number is given, the sign will be ignored
func NewDigitsWithSize(number int, height int, width int) Digits {
	digits := new(Digits)
	if number < 0 {
		number = -number
	}
	for ok := true; ok; ok = (number > 0) {
		digits.append(NewDigitWithSize(number%10, height, width))
		number /= 10
	}
	return *digits
}

// Create and return a Digits using an integer
func NewDigitsFromInt(number int) Digits {
	return NewDigitsWithSize(number, 1, 1)
}

// Fill an array of string with spaces if the Digit is smaller
func fillWithSpaces(str []string, d Digit, size int) {
	for i := d.Height(); i < size; i++ {
		for j := 0; j < d.Width(); j++ {
			str[i] += " "
		}
	}
}

// Add a Digit to an array of string
// Every part is added to the corresponding index
func addDigit(str []string, d Digit) {
	idx := 0
	for i := d.Height() - 1; i >= 0; i-- {
		str[idx] += d[i]
		idx++
	}
}

// Create and return the string representation of a Digits
func (ds *Digits) String() string {
	tmps := make([]string, ds.height)
	for i := len(ds.digits) - 1; i >= 0; i-- {
		addDigit(tmps, ds.digits[i])
		fillWithSpaces(tmps, ds.digits[i], ds.height)
	}
	res := ""
	for i := len(tmps) - 1; i >= 0; i-- {
		res += tmps[i] + "\n"
	}
	return res
}